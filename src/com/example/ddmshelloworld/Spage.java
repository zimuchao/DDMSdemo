package com.example.ddmshelloworld;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class Spage extends Activity{
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	Log.v("logv", "Spage--->onCreate");
	super.onCreate(savedInstanceState);
	setContentView(R.layout.spage);
}

@Override
protected void onDestroy() {
	// TODO Auto-generated method stub
    Log.v("logv", "Spage--->onDestroy");
    super.onDestroy();
}


@Override
protected void onPause() {
	// TODO Auto-generated method stub
    Log.v("logv", "Spage--->onPause");
	super.onPause();
	
}

@Override
protected void onRestart() {
	// TODO Auto-generated method stub
    Log.v("logv", "Spage--->onRestart");
	super.onRestart();
	
}

@Override
protected void onResume() {
	// TODO Auto-generated method stub
    Log.v("logv", "Spage--->onResume");
	super.onResume();
	
}

@Override
protected void onStart() {
	// TODO Auto-generated method stub
    Log.v("logv", "Spage--->onStart");
    super.onStart();
}

@Override
protected void onStop() {
	// TODO Auto-generated method stub
    Log.v("logv", "Spage--->onStop");
	super.onStop();
}

}
