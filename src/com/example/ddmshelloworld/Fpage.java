package com.example.ddmshelloworld;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Fpage extends Activity{
 Button btn_2;
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
    setContentView(R.layout.fpage);
    Log.v("logv", "Fpage--->onCreate");
    btn_2=(Button) findViewById(R.id.btn_2);
    btn_2.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Intent intent=new Intent();
			intent.setClass(Fpage.this, Spage.class);
			startActivity(intent);
		}
	});
}

 @Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
	    Log.v("logv", "Fpage--->onDestroy");
	    super.onDestroy();
	}


@Override
	protected void onPause() {
		// TODO Auto-generated method stub
	    Log.v("logv", "Fpage--->onPause");
		super.onPause();
		
	}

@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
	    Log.v("logv", "Fpage--->onRestart");
		super.onRestart();
		
	}

@Override
	protected void onResume() {
		// TODO Auto-generated method stub
	    Log.v("logv", "Fpage--->onResume");
		super.onResume();
		
	}

@Override
	protected void onStart() {
		// TODO Auto-generated method stub
	    Log.v("logv", "Fpage--->onStart");
	    super.onStart();
	}

@Override
	protected void onStop() {
		// TODO Auto-generated method stub
	    Log.v("logv", "Fpage--->onStop");
		super.onStop();
	}

}
